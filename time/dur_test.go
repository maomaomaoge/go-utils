package time

import (
	"fmt"
	"testing"
)

func TestToday24Dur(t *testing.T) {
	dur, err := Today24Dur()
	if err != nil {
		return
	}

	fmt.Println(dur.String())
}

package time

import "time"

// func: WaitUntil(tm time.Time, dur ...time.Duration)
// aim: 阻塞到到某一时刻, 可选参数dur是for里面的休眠时长

func WaitUntil(tm time.Time, dur ...time.Duration)  {
	durNow := time.Millisecond * 300

	if len(dur) > 0 {
		durNow = dur[0]
	}

	for {
		t := time.Now()

		if t.Sub(tm) >= 0 {
			return
		}
		time.Sleep(durNow)
	}
}


// func: WaitDur(wait time.Duration, dur ...time.Duration)
// aim: 阻塞时间段

func WaitDur(wait time.Duration, dur ...time.Duration) {
	tm := time.Now()
	// 结束时间
	date := tm.Add(wait)

	durNow := time.Millisecond * 300

	if len(dur) > 0 {
		durNow = dur[0]
	}

	for {
		t := time.Now()

		if t.Sub(date) >= 0 {
			return
		}
		time.Sleep(durNow)
	}
}
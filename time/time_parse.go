package time

import (
	"time"
)

// func: TimeStringToUnix(s string) (time.Time, error)
// aim: 字符串变为时间
func TimeStringToUnix(s string) (time.Time, error) {
	layout := "2006-01-02 15:04:05"
	location, err := time.ParseInLocation(layout, s, time.Local)
	if err != nil {
		return location, err
	}

	return location, nil
}


// func: TimeUnixToString(timeUnix int64) string
// aim: int64转化为时间字符串: "2006-01-02 15:04:05"

func TimeUnixToString(timeUnix int64) string {
	var (
		layout string
		res string
	)

	layout = "2006-01-02 15:04:05"
	res = time.Unix(timeUnix, 0).Format(layout)

	return res
}

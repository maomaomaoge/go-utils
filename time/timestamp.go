// 小数点前面是整数的时间戳，
// 秒 . 毫秒 . 微妙 纳秒
package time

import (
	"strconv"
	"time"
)

// func: MillisecondTimeStamp() float64
// aim: 获取毫秒时间戳、

func MillisecondTimeStamp(tm ...time.Time) float64 {
	var t time.Time
	if len(tm) == 0 {
		t = time.Now()
	} else {
		t = tm[0]
	}

	n := t.UnixNano()

	// 下面的 float64(n/1e3%1e6)/1e6 表示取出来毫秒的部分
	res := float64(n/1e9) +  float64(n/1e3%1e6)/1e6
	return res
}


// func: MillisecondTimeStampStr() string
// aim: 获取毫秒的字符串，有bug

func MillisecondTimeStampStr() string {
	t := time.Now()
	n := t.UnixNano()
	a := float64(n/1e3%1e6)/1e6

	sec := strconv.Itoa(int(t.Unix()))
	pa := a - float64(int(a))
	spa := strconv.Itoa(int(pa * 1e9))
	goodTime := sec + "." + spa

	return goodTime
}
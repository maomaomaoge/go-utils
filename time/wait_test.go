package time

import (
	"testing"
	"time"
)

func TestWaitUntil(t *testing.T) {
	now := time.Now()

	date := now.Add(time.Second * 10)

	WaitUntil(date)

	if time.Since(now).Seconds() < 10 {
		t.Fatal("测试失败")
	}

	t.Log(time.Since(now).String())
}


func TestWaitDur(t *testing.T) {
	three := time.Second * 3

	now := time.Now()
	WaitDur(three)
	since := time.Since(now)

	if since.Seconds() < 3 {
		t.Fatal("测试失败")
	}

	t.Log(since.String())
}
package file

import (
	"fmt"
	"testing"
)

func TestPathExists(t *testing.T) {
	exists, err := PathExists("../go.mod")
	if err != nil {
		fmt.Println(err.Error(), exists)
		return
	}

	fmt.Println(exists)
}

func TestWriteStringWithEnter(t *testing.T) {
	for i := 0;i < 3;i++ {
		err := WriteStringWithEnter("666", "a.txt")
		if err != nil {
			fmt.Println(err)
			return
		}
	}
}

func TestReadFileSplitEnter(t *testing.T) {
	var (
		res []string
		err error
	)

	res = make([]string, 0)

	fmt.Println("结果1-----------------------------： ")
	if res, err = ReadFileSplitEnter("./maob_file_test.go"); err != nil {
		fmt.Println(err)
	}
	fmt.Println(res)

	fmt.Println("结果2-----------------------------： ")
	if res, err = ReadFileSplitEnter("./maob_file_test.go2"); err != nil {
		fmt.Println(err)
	}
	fmt.Println(res)

	fmt.Println("结果3-----------------------------： ")
	if res, err = ReadFileSplitEnter("../go.mod"); err != nil {
		fmt.Println(err)
	}
	fmt.Println(res)
}
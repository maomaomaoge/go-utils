package file

import (
	"errors"
	"io/ioutil"
	"os"
	"path/filepath"
)

// func: GetDirListNoRecursion(path string) ([]string, error)
// aim: 获取目录下的所有目录名字，不包括文件

func GetDirListNoRecursion(path string) ([]string, error) {
	var (
		result       []string
		err          error
		fileInfoList []os.FileInfo
	)

	result = make([]string, 0)
	if fileInfoList, err = ioutil.ReadDir(path); err != nil {
		return nil, err
	}

	for _, v := range fileInfoList {
		if !v.IsDir() {
			continue
		}

		result = append(result, v.Name())
	}

	return result, err
}


// func: CreateDir(dirs ...string) (err error)
// aim: 批量创建文件夹

func CreateDir(dirs ...string) (err error) {
	for _, v := range dirs {
		exist, err := PathExists(v)
		if err != nil {
			return err
		}
		if !exist {
			err = os.MkdirAll(v, os.ModePerm)
		}
	}
	return err
}


// func: FileMove(src string, dst string) (err error)
// aim: 文件移动供外部调用

func FileMove(src string, dst string) (err error) {
	if dst == "" {
		return nil
	}
	src, err = filepath.Abs(src)
	if err != nil {
		return err
	}
	dst, err = filepath.Abs(dst)
	if err != nil {
		return err
	}
	var revoke = false
	dir := filepath.Dir(dst)
Redirect:
	_, err = os.Stat(dir)
	if err != nil {
		err = os.MkdirAll(dir, 0755)
		if err != nil {
			return err
		}
		if !revoke {
			revoke = true
			goto Redirect
		}
	}
	return os.Rename(src, dst)
}


// func:
// aim: 判断文件夹下面有没有任意文件, true: 存在, fasle: 不存在

func HaveFileInDir(dir string) (bool, error) {
	exist, err := PathExists(dir)
	if err != nil {
		return false, err
	}
	if !exist {
		return false, errors.New("不存在该文件夹")
	}

	readDir, err := os.ReadDir(dir)
	if err != nil {
		return false, err
	}

	for _, v := range readDir {
		// 存在文件，就返回了
		if !v.IsDir() {
			return true, nil
		}
	}

	// 不存在文件
	return false, nil
}
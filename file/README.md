1.
//@author: maob
//@function: PathExists
//@description: 获取目录下的所有目录名字，不包括文件
//@param: path string

2.
//@author: maob
//@function: PathExists
//@description: 文件目录是否存在
//@param: path string
//@return: bool, error
// @test: ExamplePathExists()

3.
//@author: maob
//@function: CreateDir
//@description: 批量创建文件夹
//@param: dirs ...string
//@return: err error
// test: TestCreateDir()

4.
//@author: maob
//@function: WriteStringWithEnter
//@description: 自动换行插入数据，没有文件并且自动创建
//@return: error

5.
//@author: maob
//@function: ReadFileSplitEnter
//@description: 读取文件，返回以换行符为分割的切片
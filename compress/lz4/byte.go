package lz4

import mlz4 "github.com/bkaradzic/go-lz4"

// func: DecodeByte(b []byte) ([]byte, error)
// aim: lz4字节解压缩, 包: github.com/bkaradzic/go-lz4

func DecodeByte(b []byte) ([]byte, error) {
	decode, err := mlz4.Decode(nil, b)
	if err != nil {
		return nil, err
	}

	return decode, nil
}


// func: EncodeByte(b []byte) ([]byte, error)
// aim: lz4字节压缩, 包: github.com/bkaradzic/go-lz4

func EncodeByte(b []byte) ([]byte, error) {
	ebuf, err := mlz4.Encode(nil, b)
	if err != nil {
		return nil, err
	}

	return ebuf, nil
}
package lz4

import (
	"bytes"
	"encoding/gob"
	"fmt"
	"log"
	"testing"
	"time"
)

func TestEncodeByte(t *testing.T) {
	type Rel struct {
		ID int32
		GN string
		TM float64
		AV interface{}
	}

	data := make([]*Rel, 0)
	ti := float64(time.Now().Unix())
	for i := 0; i < 100000;i++ {
		data = append(data, &Rel{
			ID: int32(1),
			GN: "",
			TM: ti,
			AV: i,
		})
	}

	var network bytes.Buffer // Stand-in for the network.
	enc := gob.NewEncoder(&network)
	err := enc.Encode(data)
	if err != nil {
		log.Println("encode:", err)
		return
	}

	fmt.Println("gob编码的数据大小为: ", len(network.Bytes()) / 1024)

	encodeByte, err := EncodeByte(network.Bytes())
	if err != nil {
		log.Fatalln(err)
		return
	}

	fmt.Println("lz4压缩之后大小为: ", len(encodeByte) / 1024)

	fmt.Println("相差大小为: ", (len(network.Bytes()) - len(encodeByte))/1024)
}

func TestDecodeByte(t *testing.T) {
	type Rel struct {
		ID int32
		GN string
		TM float64
		AV interface{}
	}

	data := make([]*Rel, 0)
	ti := float64(time.Now().Unix())
	for i := 0; i < 100000;i++ {
		data = append(data, &Rel{
			ID: int32(1),
			GN: "",
			TM: ti,
			AV: i,
		})
	}

	var network bytes.Buffer // Stand-in for the network.
	enc := gob.NewEncoder(&network)
	err := enc.Encode(data)
	if err != nil {
		log.Println("encode:", err)
		return
	}

	encodeByte, err := EncodeByte(network.Bytes())
	if err != nil {
		log.Fatalln(err)
		return
	}

	decodeByte, err := DecodeByte(encodeByte)
	if err != nil {
		log.Fatalln(err)
		return
	}

	data2 := make([]*Rel, 0)
	reader := bytes.NewReader(decodeByte)
	dec := gob.NewDecoder(reader)
	err = dec.Decode(&data2)
	if err != nil {
		fmt.Println("decode:", err)
		return
	}

	fmt.Println(len(data2))
}

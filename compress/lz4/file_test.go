package lz4

import "testing"

func TestSaveLz4File(t *testing.T) {
	p := "./f"
	data := []byte("test lz4")

	err := SaveLz4File(p, data)
	if err != nil {
		t.Fatal(err)
	}
}

func TestLoadLz4File(t *testing.T) {
	p := "./f"

	file, err := LoadLz4File(p)
	if err != nil {
		t.Fatal(err)
	}

	t.Log(string(file))
}
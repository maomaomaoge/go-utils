package service

import (
	"os/exec"
)

// func: WindowsKillExe(s string) error
// aim: 干掉windows相关的exe

func WindowsKillExe(s string) error {
	_, err := exec.Command("taskkill.exe", "/f", "/im", s).Output()
	if err != nil {
		return err
	}

	return nil
}

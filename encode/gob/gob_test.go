package gob

import (
	"fmt"
	"gitee.com/maomaomaoge/go-utils/shift"
	"log"
	"testing"
	"time"
)

type op struct {
	Id int32
	GN string
	TM int64
	AV interface{}
}

func mockData(l int) []*op {
	data := make([]*op, 0)
	t := time.Now().Unix()

	for i := 0;i < l;i++ {
		data = append(data, &op{
			Id: int32(1),
			GN: "W3.AA.AA",
			TM: t,
			AV: i,
		})
	}

	return data
}

func TestEncode(t *testing.T) {
	data := mockData(5000 * 1000)

	ta := time.Now()
	encode, err := Encode(data)
	if err != nil {
		log.Fatalln(err)
	}

	fmt.Println(time.Since(ta).String())

	fmt.Println(shift.ByteToMB(encode))
}

func BenchmarkEncode(b *testing.B) {
	data := mockData(5000 * 1000)

	for i := 0; i < b.N; i++ { //use b.N for looping
		_, err := Encode(data)
		if err != nil {
			panic(err)
		}
	}
}
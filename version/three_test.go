package version

import (
	"fmt"
	"log"
	"testing"
)

func TestGetLastVersion_Three(t *testing.T) {
	three, err := GetLastVersion_Three([]string{"v1.0.0", "v3.0.0", "v2.0.0", "v4.0.3", "v2.0.1"})
	if err != nil {
		log.Fatalln(err)
	}

	fmt.Println(three)
}

package scp

import (
	"time"

	SCP "github.com/tmc/scp"
	"golang.org/x/crypto/ssh"
)

func CopyFile(host, user, pwd, src, dest string) (err error) {
	var auths []ssh.AuthMethod
	auths = append(auths, ssh.Password(pwd))
	config := ssh.ClientConfig{
		Timeout:         time.Second * 30,
		User:            user,
		Auth:            auths,
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
	}

	//创建tcp连接
	conn, err := ssh.Dial("tcp", host, &config)
	if err != nil {
		return
	}
	defer conn.Close()

	//打开会话
	session, err := conn.NewSession()
	if err != nil {
		return
	}
	defer session.Close()

	err = SCP.CopyPath(src, dest, session)
	return
}

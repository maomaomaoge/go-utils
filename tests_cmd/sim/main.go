package main

import (
	"fmt"
	"io/fs"
	"path/filepath"
	"time"
)

func main() {
	n := time.Now()
	filepath.Walk("a", func(path string, info fs.FileInfo, err error) error {
		fmt.Println(path)
		return nil
	})

	s := time.Since(n).String()
	fmt.Print(s)
}

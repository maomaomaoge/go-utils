package main

import (
	"fmt"

	"github.com/shirou/gopsutil/v3/mem"
	// "github.com/shirou/gopsutil/mem"  // to use v2
)

func main() {
	data, _ := mem.VirtualMemory()

	// almost every return value is a struct
	fmt.Printf("Total: %v, Free:%v, UsedPercent:%f\n", data.Total, data.Free, data.UsedPercent)

	// fmt.Println(float64(v.Total) / 1024 / 1024 / 1024)
	// fmt.Println(float64(v.Free) / 1024 / 1024 / 1024)

	// convert to JSON. String() is also implemented
	// fmt.Println(v)
}

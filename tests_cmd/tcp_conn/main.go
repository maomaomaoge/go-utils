package main

import (
	"fmt"
	"log"
	"net"
	"time"
)

var rec int64

func main() {
	ln, err := net.Listen("tcp", ":7000")
	if err != nil {
		// handle error
	}
	for {
		conn, err := ln.Accept()
		if err != nil {
			log.Fatalln(err)
		}

		go read(conn)
		go serverWrite(conn)
	}
}

func read(conn net.Conn)  {
	for {
		data := make([]byte, 1024 * 1024  * 10)
		read, err := conn.Read(data[:])
		if err != nil {
			log.Fatalln(err)
		}

		rec += int64(read)
		fmt.Println(rec)
	}
}

func serverWrite(conn net.Conn)  {
	data := []byte{1, 2 ,3}

	for {
		_, err := conn.Write(data)
		if err != nil {
			fmt.Println("serve 给client 写数据失败")
			return
		}
		time.Sleep(time.Second)
	}

}
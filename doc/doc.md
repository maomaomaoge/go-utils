


#### buff

```

buff\buffer.go func: NewBuffer(size int) *Buffer 
buff\buffer.go aim: 小标头字节封装

```



```

buff\buffer.go func: (this *Buffer) PutByte(value byte)
buff\buffer.go aim: 放一个字节，游标移动1

```



```

buff\buffer.go func: (this *Buffer) PutUint16(pos int, value uint16) 
buff\buffer.go aim: 放 uint16 , 游标移动2

```



```

buff\buffer.go func: (this *Buffer) Uint16(pos int) (value uint16)
buff\buffer.go aim: 取出Uint16

```



```

buff\buffer.go func: (this *Buffer) PutUint32(pos int, value uint32)
buff\buffer.go aim: 放uint32 , 游标移动4

```



```

buff\buffer.go func: (this *Buffer) Uint32() (value uint32)
buff\buffer.go aim: 取出Uint32, 字节4

```



```

buff\buffer.go func: (this *Buffer)PutUint64(value uint64)
buff\buffer.go aim: 放uint64, 8字节

```



```

buff\buffer.go func: (this *Buffer)Uint64() (value uint64)
buff\buffer.go aim: 取出Uint64, 8 字节

```



```

buff\buffer.go func: (this *Buffer) PutFloat(pos int, value float32) 
buff\buffer.go aim: uint32, 4字节

```



```

buff\buffer.go func: (this *Buffer) Float32() float32
buff\buffer.go aim: 取出float32, 实质是Uint32

```



```

buff\buffer.go func: (this *Buffer)PutFloat64(value uint64)
buff\buffer.go aim: float64就是内部放的uint64， 8字节

```



```

buff\buffer.go func: (this *Buffer)Float64() float64
buff\buffer.go aim: 取出float64, 实质是Uint64

```



```

buff\buffer.go func: (this *Buffer)PutInt16(value int16)
buff\buffer.go aim: 放int16, 实质是PutUint16

```



```

buff\buffer.go func: (this *Buffer)Int`6() int`6
buff\buffer.go aim: 取出int16, 实质是实质是PutUint16

```



```

buff\buffer.go func: (this *Buffer)PutInt(value int)
buff\buffer.go aim: 放int, 实质是PutUint32

```



```

buff\buffer.go func: (this *Buffer)Int() int
buff\buffer.go aim: 取出int, 实质是实质是PutUint32

```



```

buff\buffer.go func: (this *Buffer)PutInt64(value int64)
buff\buffer.go aim: 放int64, 实质是PutUint64

```



```

buff\buffer.go func: (this *Buffer)Int64() int64
buff\buffer.go aim: 取出int64, 实质是实质是PutUint64

```



```

buff\buffer.go func:  (this *Buffer) LastArray() []byte
buff\buffer.go aim: 获取剩余字节数

```



```

buff\buffer.go func: (this *Buffer)PutHook(h Hook)
buff\buffer.go aim: hook

```






#### cmd

```

cmd\create.go	_defaultFunc = ` func:`
cmd\create.go	_defaultAim = ` aim:`

```






#### compress\lz4

```

compress\lz4\byte.go func: DecodeByte(b []byte) ([]byte, error)
compress\lz4\byte.go aim: lz4字节解压缩, 包: github.com/bkaradzic/go-lz4

```



```

compress\lz4\byte.go func: EncodeByte(b []byte) ([]byte, error)
compress\lz4\byte.go aim: lz4字节压缩, 包: github.com/bkaradzic/go-lz4

```



```

compress\lz4\file.go func: SaveLz4File(file string, data []byte) (err error)
compress\lz4\file.go aim: lz4算法保存数据到文件

```



```

compress\lz4\file.go func: LoadLz4File(file string) (data []byte, err error)
compress\lz4\file.go aim: LoadLz4File 从文件中加载

```






#### compress\targz

```

compress\targz\targz.go func: TarEncode(src, tarPath string) (err error)
compress\targz\targz.go aim: tar.gz 打包

```






#### compress\zip




#### encode\gob

```

encode\gob\gob.go func: Encode(data interface{}) ([]byte, error)
encode\gob\gob.go aim: gob方式把数据变为[]byte

```






#### file

```

file\maob_directory.go func: GetDirListNoRecursion(path string) ([]string, error)
file\maob_directory.go aim: 获取目录下的所有目录名字，不包括文件

```



```

file\maob_directory.go func: CreateDir(dirs ...string) (err error)
file\maob_directory.go aim: 批量创建文件夹

```



```

file\maob_directory.go func: FileMove(src string, dst string) (err error)
file\maob_directory.go aim: 文件移动供外部调用

```



```

file\maob_directory.go func:
file\maob_directory.go aim: 判断文件夹下面有没有任意文件, true: 存在, fasle: 不存在

```



```

file\maob_file.go func: PathExists(path string) (bool, error)
file\maob_file.go aim: 文件目录是否存在

```



```

file\maob_file.go func: WriteStringWithEnter(dataStr, fp string) error
file\maob_file.go aim: 自动换行插入数据，没有文件并且自动创建

```



```

file\maob_file.go func: ReadFileSplitEnter(filePath string) ([]string, error)
file\maob_file.go aim: 读取文件，返回以换行符为分割的切片

```



```

file\text.go func: ReadTextWithoutNT(fp string) (string, error)
file\text.go aim: 读取txet去除空格和换行,和空格

```



go-utils%s%s, goid=, j0 =  aim:19531252.5.4.32.5.4.52.5.4.62.5.4.72.5.4.82.5.4.99765625::1/128:method:scheme:status</pre>

```



```

go-utils%s:%d:%d&#xfffd;(%d, %d) func:/gid_map/uid_map0.0.0.0:00000000012345672.5.4.102.5.4.112.5.4.172006-1-248828125AcceptedArmenianBalineseBecause;BoolKindBopomofoBugineseCRITICALCayleys;Cconint;Cedilla;CherokeeClassANYConflictContinueCurveID(CyrillicDNS nameDOWNGRD DOWNGRDDSA-SHA1DecemberDevmajorDevminorDiamond;DownTee;DuployanElement;EnumKindEpsilon;EthiopicExtendeeExtenderFebruaryFullNameGIN_MODEGODEBUG=GeorgianGoStringGujaratiGurmukhiHTTP/2.0HiraganaIf-MatchIf-RangeImplies;InstFailInstRuneIsPackedIsPublicJSONNameJavaneseKatakanaKayah_LiKeyGroupKind(%d)LeftTee;Linear_ALinear_BLinknameLocationMD5+SHA1MahajaniMapValueMessagesNO_ERRORNO_PROXYNewLine;NoBreak;NotLess;NovemberOl_ChikiOmicron;OptionalOverBar;PRIORITYParseIntPhags_PaProduct;QuestionReceivedRepeatedRequiredSETTINGSSHA1-RSASHA3-224SHA3-256SHA3-384SHA3-512SaturdayServicesTagbanwaTai_ThamTai_VietThursdayTifinaghTrailer:TypeAAAATypeAXFRUgariticUpArrow;Uparrow;Upsilon;ZONEINFOZgotmplZ[%d]: %s[::1]:53[:word:][signal \\([^$])



#### hash

```

hash\md5.go func: MD5V(str []byte) string
hash\md5.go aim: md5加密

```






#### kafka_pool

```

kafka_pool\kafka.go func: NewKPool(option ...Option) *KPool
kafka_pool\kafka.go aim: 未完成

```






#### math

```

math\fetch.go func: FetchUnitsDigit() (int, error)
math\fetch.go aim: 取出个位数字

```



```

math\math.go func: Round(val float64, places int) float64
math\math.go aim: 保留小数

```






#### nety\ntp

```

nety\ntp\ntp.go func: GetNetworkTime(server string) (*time.Time, error)
nety\ntp\ntp.go aim: 获取服务时间

```






#### nety\scp




#### shift

```

shift\bytesize.go func: ByteToB(data []byte) float64
shift\bytesize.go aim: 字节转化为 B

```



```

shift\bytesize.go func: ByteToKB(data []byte) float64
shift\bytesize.go aim: 字节转化为 KB

```



```

shift\bytesize.go func: ByteToMB(data []byte) float64
shift\bytesize.go aim: 字节转化为 MB

```



```

shift\bytesize.go func: ByteToGB(data []byte) float64
shift\bytesize.go aim: 字节转化为 KB

```



```

shift\struct_to_map.go func: tructToMap(obj interface{}) map[string]interface{}
shift\struct_to_map.go aim: 利用反射将结构体转化为map

```



```

shift\toFloat.go func: ToFloat64(i interface{}) (float64, error)
shift\toFloat.go aim: ToFloat64E casts an interface to a float64 type.

```



```

shift\toFloat.go func: ToFloat32(i interface{}) (float32, error)
shift\toFloat.go aim: ToFloat32E casts an interface to a float32 type.

```



```

shift\toInt.go func: ToInt64(i interface{}) (int64, error)
shift\toInt.go aim: ToInt64E casts an interface to an int64 type.

```



```

shift\toInt.go func: ToInt32(i interface{}) (int32, error)
shift\toInt.go aim: ToInt32E casts an interface to an int32 type.

```



```

shift\toInt.go func: ToInt16(i interface{}) (int16, error)
shift\toInt.go aim: ToInt16E casts an interface to an int16 type.

```



```

shift\toInt.go func: ToInt8(i interface{}) (int8, error)
shift\toInt.go aim: ToInt8E casts an interface to an int8 type.

```



```

shift\toInt.go func: ToInt(i interface{}) (int, error)
shift\toInt.go aim: ToIntE casts an interface to an int type.

```



```

shift\toString.go func: ToString(i interface{}) (string, error)
shift\toString.go aim: ToStringE casts an interface to a string type.

```



```

shift\toUint.go func: ToUint64(i interface{}) (uint64, error)
shift\toUint.go aim: ToUint64E casts an interface to a uint64 type.

```






#### system\service

```

system\service\windows.go func: WindowsKillExe(s string) error
system\service\windows.go aim: 干掉windows相关的exe

```






#### time

```

time\dur.go func: Today24Dur() (time.Duration, error)
time\dur.go aim: 获取今天半夜12点到现在的时间间隔

```



```

time\next.go func: NextIntSecond() time.Duration
time\next.go aim: 下一个整数秒的时间间隔

```



```

time\time_parse.go func: TimeStringToUnix(s string) (time.Time, error)
time\time_parse.go aim: 字符串变为时间

```



```

time\time_parse.go func: TimeUnixToString(timeUnix int64) string
time\time_parse.go aim: int64转化为时间字符串: "2006-01-02 15:04:05"

```



```

time\timestamp.go func: MillisecondTimeStamp() float64
time\timestamp.go aim: 获取毫秒时间戳、

```



```

time\timestamp.go func: MillisecondTimeStampStr() string
time\timestamp.go aim: 获取毫秒的字符串，有bug

```



```

time\wait.go func: WaitUntil(tm time.Time, dur ...time.Duration)
time\wait.go aim: 阻塞到到某一时刻, 可选参数dur是for里面的休眠时长

```



```

time\wait.go func: WaitDur(wait time.Duration, dur ...time.Duration)
time\wait.go aim: 阻塞时间段

```






#### version

```

version\three.go func: GetLastVersion_Three(data []string) string
version\three.go aim: 获取最大的版本号, 比如 v2.0.0 > v1.1.1, v可以大小写，可以不写

```




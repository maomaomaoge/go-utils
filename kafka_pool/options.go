package kafka_pool

type Option func(k *KPool)

// 是否启动订阅
func WithSub() Option  {
	return func(k *KPool) {
		k.HaveSub = true
	}
}

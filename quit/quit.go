package quit

import (
	"fmt"
	"os"
	"os/signal"
	"syscall"
)

func WaitSignal() {
	c := make(chan os.Signal, 1)
	signal.Notify(c, syscall.SIGTERM, syscall.SIGQUIT, syscall.SIGINT)
	var s os.Signal
	AddOne()
	defer DoneOne()
	for {
		select {
		case s = <-c:
			Cancel()
			fmt.Println("got os signal ", s.String())
			return
		case <-Over().Done():
			return
		}
	}

}

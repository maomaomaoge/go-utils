// Package cmd 融合一下linux系统命令
// - gus linux io speed 测试磁盘读写速度
// todo: 没实现
// - gus linux who 查看自己主机是centos还是乌班图
package cmd

import (
	"fmt"
	"gitee.com/maomaomaoge/go-utils/file"
	"gitee.com/maomaomaoge/go-utils/gio"
	"github.com/spf13/cobra"
	"io/fs"
	"os"
	"os/exec"
	"path/filepath"
)

// linuxCmd represents the linux command
var linuxCmd = &cobra.Command{
	Use:   "linux [io/env]",
	Short: "linux系统运维小工具",
	Long: `
- io speed : io读写速度 mb/s
- env：
	run: 启动自定义的环境变量
	add path: 添加临时环境变量，主要为了tab补全使用
	del path: 删除临时环境变量，主要为了tab补全使用
	find: 查看当前的环境变量
	clean: 清楚当前目录下的临时变量
`,
	Run: func(cmd *cobra.Command, args []string) {
		dir := "/usr/local/gus"
		os.Mkdir(dir, 0777)

		switch args[0] {
		case "io":
			r, w, err := gio.DiskIoSpeed()
			if err != nil {
				return
			}

			fmt.Println("磁盘读取速度位: mb/s ", r)
			fmt.Println("磁盘写速度位: mb/s ", w)

		case "env":
			switch args[1] {
			case "run":
				// 先检测文件甜美添加环境变量
				s := "/usr/local/gus"
				filepath.Walk(s, func(path string, info fs.FileInfo, err error) error {
					os.Create(info.Name())
					exec.Command("chmod", "+x", info.Name()).Run()
					file.WriteStringWithEnter("gus", info.Name())
					return nil
				})

				fmt.Println("环境变量设置ok: ", s)

			case "add":
				f := dir + "/" + args[2]
				os.Create(f)
				exec.Command("chmod", "+x", f).Run()
				file.WriteStringWithEnter("gus", f)

			case "del":
				f := dir + "/" + args[2]
				os.RemoveAll(f)

			case "clean":
				s := "/usr/local/gus"
				filepath.Walk(s, func(path string, info fs.FileInfo, err error) error {
					os.RemoveAll(info.Name())
					return nil
				})

			default:
				fmt.Println("没有策略")
			}

		default:
			fmt.Println("匹配不到")
		}
	},
}

func init() {
	rootCmd.AddCommand(linuxCmd)
}



// Package cmd 命令测试
// - test
// -test sb 判断是bash 模式 还是 sh 模式 ， 未实现
package cmd

import (
	"fmt"
	"gitee.com/maomaomaoge/go-utils/file"
	"github.com/spf13/cobra"
	"time"
)

// testCmd represents the test command
var testCmd = &cobra.Command{
	Use:   "test [iow] [file]",
	Short: "自测",
	Long: `
test iow file 每秒写时间戳到指定的文件
test sb  判断当前支持bash还是sh解释器
`,
	Run: func(cmd *cobra.Command, args []string) {
		ticker := time.NewTicker(time.Second)
		for {
			t := <-ticker.C
			file.WriteStringWithEnter(t.String(), args[1])
			fmt.Println(t.String())
		}
	},
}

func init() {
	rootCmd.AddCommand(testCmd)
}

package shift

// func: ByteToB(data []byte) float64
// aim: 字节转化为 B

func ByteToB(data []byte) float64  {
	return float64(len(data))
}


// func: ByteToKB(data []byte) float64
// aim: 字节转化为 KB

func ByteToKB(data []byte) float64  {
	return float64(len(data))/float64(1024)
}


// func: ByteToMB(data []byte) float64
// aim: 字节转化为 MB

func ByteToMB(data []byte) float64  {
	return float64(len(data))/float64(1024 * 1024)
}


// func: ByteToGB(data []byte) float64
// aim: 字节转化为 KB

func ByteToGB(data []byte) float64  {
	return float64(len(data))/float64(1024 * 1024 * 1024)
}

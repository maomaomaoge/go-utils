package shift

import (
	"fmt"
	"testing"
)

type Person struct {
	Child *Person
	Name string
	Age int
	Likes []string
	//Chilrens []*Person
}

func newPerson() Person {
	p :=  Person{
		Child:&Person{
			Child: nil,
			Name:  "MAOB2",
			Age:   2,
			Likes: make([]string, 0),
		},
		Name:  "maob",
		Age:   1,
		Likes: make([]string, 0),
	}

	p.Child.Likes = append(p.Child.Likes, "3", "3")
	p.Likes = append(p.Likes, "1", "2")

	return p
}

func TestStructToMap(t *testing.T) {
	person := newPerson()
	toMap := StructToMap(person)
	fmt.Println(toMap)
}
